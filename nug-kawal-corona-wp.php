<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

/**
	* Plugin Name: NUG Kawal Corona WP
	* Plugin URI: https://www.nugweb.id/plugin/
	* Author: NUG Website
	* Author URI: https://nugweb.id
	* Version: 1.05.04.20
	* Description: Kawal Corona adalah sebuah website yang memberikan informasi data live untuk monitoring update terbaru kasus virus Corona Covid-19 di Indonesia dan Dunia. Plugin sederhana ini berguna untuk membantu bagi yang ingin memasang data statistik kasus tersebut di website berbasis Wordpress masing-masing.<br><strong>Terima kasih:</strong>Ethical Hacker Indonesia (kawalcorona.com), Webinasia (webinasia.com), Jogja web (jogjaweb.co.id), Iwan R (hanyalewat.com)
	* License: GPLv2 or later
	* License URI: http://www.gnu.org/licenses/gpl-2.0.txt
	* Text Domain: nug-kawal-corona
	*/

defined('ABSPATH') or die("Restricted access!");

/**
 * Define constants
 *
 * @since 1.03.20
 */
defined('NUGKC_DIR') or define('NUGKC_DIR', dirname(plugin_basename(__FILE__))); //Folder Name
defined('NUGGKCBASE') or define('NUGGKCBASE', plugin_basename(__FILE__));
defined('NUGKC_URL') or define('NUGKC_URL', plugin_dir_url(__FILE__));
defined('NUGGKCPATH') or define('NUGGKCPATH', plugin_dir_path(__FILE__));

/**
 * Revised by Iwan R. <iwan@jogjaweb.co.id> 2020-04-05
**/

// Enqueue script and call when shortcode called
function nugkc_enqueue()
{
	wp_register_style( 'nugkc-style', NUGKC_URL . 'assets/css/style.css' );
}
add_action( 'wp_enqueue_scripts', 'nugkc_enqueue' );

// Load file on plugins loaded
function nug_kawal_corona_on_loaded()
{
	// load file with functions
	$controller_script = [
		'kawalcorona',
		'shortcodes'
	];
	foreach( $controller_script as $cs ) {
		require_once __DIR__ . '/includes/' . $cs . '.php';
	}
}
add_action( 'plugins_loaded', 'nug_kawal_corona_on_loaded' );
