<?php
/* SHORTCODE */

// Shortcode for display kawalcorona
function nugkc_data_sc_callback($atts){

	extract(shortcode_atts(array(
		'data' 	=> 'global', //global, indonesia
		'provinsi' => null, //menggunakan nama provinsi sesuai tabel
		'style'	=> 'table', //table, card
		'class_wrap' => '', //class untuk tabel atau class untuk container
		'id_wrap' => '', //id untuk tabel atau id untuk container
		'credit_text' => true,
		'credit_nug' => true,
	), $atts));

	$contents = nug_display_kawalcorona( $data, $provinsi, $style, $class_wrap, $id_wrap, $credit_text, $credit_nug );
	return $contents;
}


function nugkc_register_shortcodes(){
	add_shortcode('nug_data_corona', 'nugkc_data_sc_callback');
}

add_action( 'init', 'nugkc_register_shortcodes');
