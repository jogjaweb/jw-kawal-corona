<?php

// function curl by Iwan R. <iwan@jogjaweb.co.id>
function nug_curl_get_data( $url ) {
	$cookie_file = __DIR__ . '/cookies.txt';
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $url );
	if( file_exists( $cookie_file ) ) {
		curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookie_file ); // request menggunakan cookies supaya tidak dianggap anonim
	} else {
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie_file ); // simpan cookie jika baru pertama
	}
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 15 );
	$contents = curl_exec( $ch );
	curl_close($ch);

	return $contents;
}

// function transient caching by Iwan R. <iwan@jogjaweb.co.id>
function nug_get_kawalcorona_data( $endpoint ) {
	$api_url = 'https://api.kawalcorona.com';
	$endpoint_url = $api_url . '/' . $endpoint . '/';
	$transient = '_transient_' . md5( $endpoint_url );
	$arr_data = get_transient( $transient );
	if( !$arr_data ) {
		$arr_data = nug_curl_get_data( $endpoint_url );
		set_transient( $transient, $arr_data, HOUR_IN_SECONDS );
	}
	if( $arr_data ) $arr_data = json_decode( $arr_data );
	return $arr_data;
}

// function to display data

function nug_display_kawalcorona( $data = 'global', $provinsi = null, $style = 'table', $class_wrap = '', $id_wrap = '', $credit_text = true, $credit_nug = true ) {
	wp_enqueue_style( 'nugkc-style' );
	$content = '';
	$data = ($data == 'global' || empty($data)) ? '' : $data;
	$credit_nug = ($credit_nug != false) ? ' <a href="https://www.nugweb.id/plugin/nug-kawal-corona-wp/" target="_blank" class="info-wrap"><span class="icon"></span><span class="text">Baca disini untuk pasang di Wordpress</span></a>' : '';
	$credit= '<p class="sumber_data">Sumber Data: Kementerian Kesehatan & JHU melalui API <a href="https://kawalcorona.com/" target="_blank">Kawal Corona</a>'.$credit_nug.'</p>';

	if ($style == 'table' && ($data != 'positif' || $data != 'sembuh' || $data != 'meninggal' )) {
		$data = ($data == 'indonesia') ? 'indonesia/provinsi' : $data;
		$data_arr = nug_get_kawalcorona_data( $data );
		$th_lokasi = ($data == 'indonesia/provinsi') ? 'Provinsi' : 'Negara';
		$id_wrap = !empty($id_wrap) ? 'id="'.$id_wrap.'"' : '';
		$content .= '
			<div class="table-responsive">
				<table '.$id_wrap.' class="table '.$class_wrap.'">
					<thead>
						<tr>
							<th class="no">No</th>
							<th class="lokasi">'.$th_lokasi.'</th>
							<th class="positif">Positif</th>
							<th class="sembuh">Sembuh</th>
							<th class="meninggal">Meninggal</th>';
						$content .= '
						</tr>
					</thead>
					<tbody>';
						$no = 1;
						foreach ($data_arr as $data_kasus) {
							$data_kasus = $data_kasus->attributes;
							$data_lokasi = ($data == 'indonesia/provinsi') ? $data_kasus->Provinsi : $data_kasus->Country_Region;
							$data_positif = ($data == 'indonesia/provinsi') ? $data_kasus->Kasus_Posi : $data_kasus->Confirmed;
							$data_sembuh = ($data == 'indonesia/provinsi') ? $data_kasus->Kasus_Semb : $data_kasus->Recovered;
							$data_meninggal = ($data == 'indonesia/provinsi') ? $data_kasus->Kasus_Meni : $data_kasus->Deaths;
							$content .= '<tr>
								<td>'.$no.'</td>
								<td>'.$data_lokasi.'</td>
								<td>'.number_format($data_positif, 0, ',', '.').'</td>
								<td>'.number_format($data_sembuh, 0, ',', '.').'</td>
								<td>'.number_format($data_meninggal, 0, ',', '.').'</td>';
							$content .= '</tr>';
							$no++;
						}
					$content .= '
					</tbody>
				</table>
			</div>
			';
			if($credit_text == true){ $content .= $credit; }
	}elseif($style == 'card'){
		if (empty($data) || $data == 'global') {
			//positif
			$data_arr_positif = nug_get_kawalcorona_data( 'positif' );
			$total_positif = str_replace(",",".",$data_arr_positif->value);

			//sembuh
			$data_arr_sembuh = nug_get_kawalcorona_data( 'sembuh' );
			$total_sembuh = str_replace(",",".",$data_arr_sembuh->value);

			//meninggal
			$data_arr_meninggal = nug_get_kawalcorona_data( 'meninggal' );
			$total_meninggal = str_replace(",",".",$data_arr_meninggal->value);

			$id_wrap = !empty($id_wrap) ? 'id="'.$id_wrap.'"' : '';

			$content = '
			<div '.$id_wrap.' class="'.$class_wrap.'">
				<div class="row">
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<div class="card bg-danger img-card box-primary-shadow">
							<div class="card-body">
								<div class="d-flex">
									<div class="text-white">
										<p class="mb-0 pb-0">TOTAL POSITIF</p>
										<h2 class="pb-0 mb-0 number-font">'.$total_positif.'</h2>
										<p class="mb-0 pb-0">ORANG</p>
									</div>
									<div class="ml-auto"> <img src="'.NUGKC_URL.'/assets/images/sad-u6e.png" width="50" height="50" alt="Positif"> </div>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<div class="card bg-success img-card box-secondary-shadow">
							<div class="card-body">
								<div class="d-flex">
									<div class="text-white">
										<p class="mb-0 pb-0">TOTAL SEMBUH</p>
										<h2 class="pb-0 mb-0 number-font">'.$total_sembuh.'</h2>
										<p class="mb-0 pb-0">ORANG</p>
									</div>
									<div class="ml-auto"> <img src="'.NUGKC_URL.'/assets/images/happy-ipM.png" width="50" height="50" alt="Positif"> </div>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<div class="card  bg-secondary img-card box-success-shadow">
							<div class="card-body">
								<div class="d-flex">
									<div class="text-white">
										<p class="mb-0 pb-0">TOTAL MENINGGAL</p>
										<h2 class="pb-0 mb-0 number-font">'.$total_meninggal.'</h2>
										<p class="mb-0 pb-0">ORANG</p>
									</div>
									<div class="ml-auto"> <img src="'.NUGKC_URL.'/assets/images/emoji-LWx.png" width="50" height="50" alt="Positif"> </div>
								</div>
							</div>
						</div>
					</div><!-- COL END -->';
					if($credit_text == true){ $content .= '<div class="col text-center">'.$credit.'</div>'; }
				$content .= '</div>
			</div>
			';
		}elseif($data == 'indonesia'){
			if (empty($provinsi)) {
				$data_arr_id = nug_get_kawalcorona_data( 'indonesia' );
				$total_positif = str_replace(",","",$data_arr_id[0]->positif);
				$total_positif = number_format($total_positif, 0, ',', '.');
				$total_sembuh = str_replace(",","",$data_arr_id[0]->sembuh);
				$total_sembuh = number_format($total_sembuh, 0, ',', '.');
				$total_meninggal = str_replace(",","",$data_arr_id[0]->meninggal);
				$total_meninggal = number_format($total_meninggal, 0, ',', '.');
			}else{
				$data_arr_id = nug_get_kawalcorona_data( 'indonesia/provinsi' );
				foreach ($data_arr_id as $data_kasus) {
					$data_kasus = $data_kasus->attributes;
					$data_prov = $data_kasus->Provinsi;
					if ($data_prov == $provinsi) {
						$total_positif = number_format($data_kasus->Kasus_Posi, 0, ',', '.');
						$total_sembuh = number_format($data_kasus->Kasus_Semb, 0, ',', '.');
						$total_meninggal = number_format($data_kasus->Kasus_Meni, 0, ',', '.');
					}
				}
			}

			$id_wrap = !empty($id_wrap) ? 'id="'.$id_wrap.'"' : '';

			$content = '
			<div '.$id_wrap.' class="'.$class_wrap.'">
				<div class="row">
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<div class="card bg-danger img-card box-primary-shadow">
							<div class="card-body">
								<div class="d-flex">
									<div class="text-white">
										<p class="mb-0 pb-0">TOTAL POSITIF</p>
										<h2 class="pb-0 mb-0 number-font">'.$total_positif.'</h2>
										<p class="mb-0 pb-0">ORANG</p>
									</div>
									<div class="ml-auto"> <img src="'.NUGKC_URL.'/assets/images/sad-u6e.png" width="50" height="50" alt="Positif"> </div>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<div class="card bg-success img-card box-secondary-shadow">
							<div class="card-body">
								<div class="d-flex">
									<div class="text-white">
										<p class="mb-0 pb-0">TOTAL SEMBUH</p>
										<h2 class="pb-0 mb-0 number-font">'.$total_sembuh.'</h2>
										<p class="mb-0 pb-0">ORANG</p>
									</div>
									<div class="ml-auto"> <img src="'.NUGKC_URL.'/assets/images/happy-ipM.png" width="50" height="50" alt="Positif"> </div>
								</div>
							</div>
						</div>
					</div><!-- COL END -->
					<div class="col-sm-12 col-md-6 col-lg-6 col-xl-4">
						<div class="card  bg-secondary img-card box-success-shadow">
							<div class="card-body">
								<div class="d-flex">
									<div class="text-white">
										<p class="mb-0 pb-0">TOTAL MENINGGAL</p>
										<h2 class="pb-0 mb-0 number-font">'.$total_meninggal.'</h2>
										<p class="mb-0 pb-0">ORANG</p>
									</div>
									<div class="ml-auto"> <img src="'.NUGKC_URL.'/assets/images/emoji-LWx.png" width="50" height="50" alt="Positif"> </div>
								</div>
							</div>
						</div>
					</div><!-- COL END -->';
					if($credit_text == true){ $content .= '<div class="col text-center">'.$credit.'</div>'; }
				$content .= '</div>
			</div>
			';
		}else{
			$content = 'Data tidak ada';
		}
	}

	return $content;
}
